<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipReplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_replacements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('characterOwnerHash');
            $table->string('inGameName');
            $table->string('shipType');
            $table->string('zKillBoardLink');
            $table->string('fleetDate');
            $table->string('fcName');
            $table->string('killID');
            $table->string('solarSystemID');
            $table->string('totalValue');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ship_replacements');
    }
}
