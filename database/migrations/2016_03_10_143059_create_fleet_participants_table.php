<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFleetParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleet_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fleet_id');
            $table->string('characterName');
            $table->string('location');
            $table->string('shipType');
            $table->string('shipGroup');
            $table->string('fleetRole');
            $table->string('fleetSkills');
            $table->string('fleetPosition');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fleets');
    }
}
