<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Session;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $group = User::where('characterOwnerHash', Session::get('ownerHash'))->first();

        if($group->group === 'admin'){
            return $next($request);
        }

        return redirect('welcome');

    }
}
