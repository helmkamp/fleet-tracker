<?php

namespace app\Http\Connection;

Interface ConnectionInterface {
    public function connect($options);
}