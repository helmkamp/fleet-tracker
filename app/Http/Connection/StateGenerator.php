<?php

namespace App\Http\Connection;

Class StateGenerator {

    private $length;

    public function __construct($length)
    {
        $this->setLength($length);
    }

    private function setLength($length) {
        $this->length = $length;
    }

    public function stateGenerator($keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        if (!defined('PHP_VERSION_ID')) {
            $RandomCompatversion = explode('.', PHP_VERSION);
            define(
            'PHP_VERSION_ID',
                $RandomCompatversion[0] * 10000
                + $RandomCompatversion[1] * 100
                + $RandomCompatversion[2]
            );
            $RandomCompatversion = null;
        }
        if(PHP_VERSION_ID >= 70000) {
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $this->length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
            return $str;
        }

        return substr( md5(rand()), 0, 7);
    }
}