<?php

namespace App\Http\Connection;

use Illuminate\Support\Facades\Session;



class Killmail {

    private $url;

    public function __construct($killID)
    {
        $this->url = $this->setURL($killID);

    }

    private function setURL($killID) {

        return $url = "https://zkillboard.com/api/killID/" . $killID . "/no-items/no-attackers/";
        
    }

    private function getUrl()
    {
        return $this->url;
    }

    public function connect() {
        $url = $this->getUrl();
        $info = (new KillboardCurl())->connect($url);
        return $info;
    }


}
