<?php


namespace App\Http\Connection;


class KillmailForSrp
{

    public function __construct()
    {
        
    }

    public function buildUrl($characterID, $startTime, $endTime) {
        $times = ["startTime" => $startTime, "endTime" => $endTime];
        $strippedTime = $this->timeFormat($times);
        $url = "https://zkillboard.com/api/losses/characterID/"
            . $characterID
            . "/startTime/"
            . $strippedTime[0]
            ."/endTime/"
            . $strippedTime[1]
            ."/no-items/no-attackers/";
        return (new KillboardCurl())->connect($url);
    }

    private function timeFormat($times){
        $pattern = '/(\D+)/';
        $newTimes = array();
        foreach ($times as $time) {
            $newTime = preg_replace($pattern, "", $time);
            array_push($newTimes, $newTime);
        }
        return $newTimes;
    }
}