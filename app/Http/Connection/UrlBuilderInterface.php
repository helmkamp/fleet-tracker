<?php

namespace App\Http\Connection;

Interface UrlBuilderInterface {
    public function builder();
}