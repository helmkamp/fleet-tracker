<?php


namespace App\Http\Connection;


class KillboardCurl
{
    public function __construct()
    {
        
    }

    public function connect($url) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(

                "User-Agent: Fleet Tracker Contact:helmy@threebeersaday.com"
            )
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error == null) {
            // Decode the response from the cURL request
            $json = json_decode($output, true);
            //dd($json);
            return $json;


        } else {
            // Instead of throwing, consider logging error and returning a default value
            throw new \Exception($error);
        }
    }
}