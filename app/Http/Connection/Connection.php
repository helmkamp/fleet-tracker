<?php

namespace App\Http\Connection;


class Connection implements ConnectionInterface{
    public function __construct()
    {

    }

    public function connect($options) {

        $ch = curl_init();

        curl_setopt_array($ch, $options);

        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error == null) {
            // Decode the response from the cURL request
            $json = json_decode($output, true);
            //dd($json);
            return $json;


        } else {
            // Instead of throwing, consider logging error and returning a default value
            throw new \Exception($error);
        }
    }
}