<?php

namespace App\Http\Connection;



class LoginUrlBuilder implements UrlBuilderInterface {


    protected $state;

    public function builder()
    {
        // Generate a random state
        $this->state = (new StateGenerator(19))->stateGenerator();

        return 'https://login.eveonline.com/oauth/authorize/?response_type=code&redirect_uri='
        . env('REDIR_URL')
        . '&client_id='
        . env('CLIENT_ID')
        . '&scope='
        . env('EVE_SCOPE')
        . '&state='
        . $this->state;
    }
}