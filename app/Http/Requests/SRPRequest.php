<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SRPRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zKillBoardLink' => array(
                'required',
                'regex:/\bhttps:\/\/zkillboard\.com\/kill\/\b\d{8}\/$/'
            ),
            'fcName' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'zKillBoardLink.required' => 'A link to your zKillBoard report is required.',
            'zKillBoardLink.regex' => 'The zKillBoard link is not a valid format.',
            'fcName.required' => 'The fleet commander name is required.'
        ];
    }
}
