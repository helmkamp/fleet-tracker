<?php


namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Session;
use App\Http\Requests;

class LoginControllerCopy extends Controller
{


    public function index(){

        session_start();

        $provider = new \Evelabs\OAuth2\Client\Provider\EveOnline([
            /*'clientId'          => '78447a54e1ca4739acb2b786a3822ad2',
            'clientSecret'      => 'YLj2cXXRul3vB2qgZbNWOyv68Z21E2YoNlwGefbU',
            'redirectUri'       => 'https://mytvindex.com/login',*/
            'redirectUri'       => 'https://fleet.localhost/login',
            'clientSecret'      => 'k3THAEx0ru70A6wR5M9FQS7Oc9ceo9t4cFo4DYPJ',
            'clientId'          => '5e38d1626f324692b6e58f41e0679df2',
        ]);

        if (!isset($_GET['code'])) {

            // here we can set requested scopes but it is totally optional
            // make sure you have them enabled on your app page at
            // https://developers.eveonline.com/applications/
            $options = [
                'scope' => ['publicData','characterLocationRead'] // array or string
            ];

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl($options);
            $_SESSION['oauth2state'] = $provider->getState();
            unset($_SESSION['token']);
            header('Location: '.$authUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

            unset($_SESSION['oauth2state']);
            exit('Invalid state');

        } else {
            // In this example we use php native $_SESSION as data store
            if(!isset($_SESSION['token']))
            {
                // Try to get an access token (using the authorization code grant)
                $_SESSION['token'] = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

            }elseif($_SESSION['token']->hasExpired()){
                // This is how you refresh your access token once you have it
                $new_token = $provider->getAccessToken('refresh_token', [
                    'refresh_token' => $_SESSION['token']->getRefreshToken()
                ]);
                // Purge old access token and store new access token to your data store.
                $_SESSION['token'] = $new_token;
            }

            // Optional: Now you have a token you can look up a users profile data
            try {

                // We got an access token, let's now get the user's details
                $data = $provider->getResourceOwner($_SESSION['token']);


                $request = $provider->getAuthenticatedRequest(
                    'GET',
                    'https://crest-tq.eveonline.com/characters/'.$data->getCharacterID().'/',
                    $_SESSION['token']
                );


                $response = $provider->getResponse($request);
                //Session::put('charName', $response['name']); /*$_SESSION['charName'] = $response['name'];*/

                //dd($data);
                return view('welcome', compact('response'));
            } catch (\Exception $e) {

                // Failed to get user details
                exit('Oh dear...'.$e);
            }

        }
    }
}

/*

https://login.eveonline.com
/oauth/authorize/
    ?response_type=code
    &redirect_uri=https://3rdpartysite.com/callback/
    &client_id=3rdpartyClientId
    &scope=
    &state=uniquestate123

*/


/*$client = new Client([
            'base_uri' => 'https://login.eveonline.com'
        ]);
        $response = $client->requestAsync(
            'GET',
            '/oauth/authorize/',
            [
                'query' => [
                    'response_type' => 'code',
                    'redirect_uri' => 'https://fleet.localhost/welcome',
                    'client_id' => '5e38d1626f324692b6e58f41e0679df2',
                    'scope' => 'publicData characterLocationRead',
                    'state' => 'someGibberish'
                ]]
        )->then(function ($response) {
            echo $response->getBody();
        });
        $response->wait();*/
// $result = (string)$response->getBody();
/* $client = new Client();

 $request = new Request(
     'GET',
     'https://login.eveonline.com/oauth/authorize',
     ['query' => [
         'response_type' => 'code',
         'redirect_uri' => 'https://fleet.localhost/login',
         'client_id' => '5e38d1626f324692b6e58f41e0679df2',
         'scope' => 'publicData characterLocationRead',
         'state' => 'someGibberish'
     ]]
     );
 $response = $client->send($request);*/


//return view('index');
//$body = json_decode($response->getBody());
//        $headers = $response->getHeaders();
//dd($result);

/*$request = new Request(
    'GET',
    'https://login.eveonline.com/oauth/authorize',
    ['query' => [
        'response_type' => 'code',
        'redirect_uri' => 'https://fleet.localhost/login',
        'client_id' => '5e38d1626f324692b6e58f41e0679df2',
        'scope' => 'publicData characterLocationRead',
        'state' => 'someGibberish'
    ]]
);
$promise = $client->sendAsync($request);
    /*->then(function ($response) {
    echo $response->get
});
$promise->wait();*/

/*
session_start();

$provider = new \Evelabs\OAuth2\Client\Provider\EveOnline([
    /*'clientId'          => '78447a54e1ca4739acb2b786a3822ad2',
    'clientSecret'      => 'YLj2cXXRul3vB2qgZbNWOyv68Z21E2YoNlwGefbU',
    'redirectUri'       => 'https://mytvindex.com/login',
    'redirectUri'       => 'https://fleet.localhost/login',
    'clientSecret'      => 'k3THAEx0ru70A6wR5M9FQS7Oc9ceo9t4cFo4DYPJ',
    'clientId'          => '5e38d1626f324692b6e58f41e0679df2',
]);

if (!isset($_GET['code'])) {

    // here we can set requested scopes but it is totally optional
    // make sure you have them enabled on your app page at
    // https://developers.eveonline.com/applications/
    $options = [
        'scope' => ['publicData', 'characterLocationRead'] // array or string
    ];

    // If we don't have an authorization code then get one
    $authUrl = $provider->getAuthorizationUrl($options);
    $_SESSION['oauth2state'] = $provider->getState();
    unset($_SESSION['token']);
    header('Location: ' . $authUrl);
    exit;

    // Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {
    // In this example we use php native $_SESSION as data store
    if (!isset($_SESSION['token'])) {
        // Try to get an access token (using the authorization code grant)
        $_SESSION['token'] = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);

    } elseif ($_SESSION['token']->hasExpired()) {
        // This is how you refresh your access token once you have it
        $new_token = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $_SESSION['token']->getRefreshToken()
        ]);
        // Purge old access token and store new access token to your data store.
        $_SESSION['token'] = $new_token;
    }

    // Optional: Now you have a token you can look up a users profile data
    try {

        // We got an access token, let's now get the user's details
        $user = $provider->getResourceOwner($_SESSION['token']);

        $request = $provider->getAuthenticatedRequest(
            'GET',
            'https://crest-tq.eveonline.com/characters/'.$user->getCharacterID().'/',
            $_SESSION['token']
        );
        $response = $provider->getResponse($request);
        $corpName = $response['corporation']['name'];
        $name = $response['name'];

        if(($corpName === 'Moosearmy') || ($corpName === 'Wafahyadoon')) {
            Session::put('charName', $user->getCharacterName());
            Session::put('charId', $user->getCharacterID());
            Session::put('ownerHash', $user->getCharacterOwnerHash());
            $this->saveUser($user);
            return view('welcome');
        } else {
            return redirect('/errors/wrongcorp')->with('name', $name);
        }

    } catch (\Exception $e) {

        // Failed to get user details
        exit('Oh dear...');
    }
}*/


/*
$code = $_GET['code'];
$body = 'grant_type=authorization_code&code=' . $code;
$b64En = base64_encode($this->clientID . ':' . $this->client_secret);

$client = new Client(
    ['base_uri' => 'https://login.eveonline.com'],
    ['headers' => [
        'Authorization' => 'Basic ' . $b64En,
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Host' => 'login.eveonline.com'
    ]]
);
$request = $client->request('POST', '/oauth/token', ['body' => $body]);
$response = $request->getBody();*/
