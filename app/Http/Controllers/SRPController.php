<?php

namespace App\Http\Controllers;

use App\Http\Connection\KillmailForSrp;
use App\Http\Connection\Killmail;
use App\Mapsolarsystems;
use Illuminate\Http\Request;
use App\ShipReplacement;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Invtype;
use Illuminate\Pagination\Paginator;
use App\FleetParticipant;
use App\Fleet;

class SRPController extends Controller
{
    public function index() {
        $srps = ShipReplacement::where('characterOwnerHash', Session::get('ownerHash'))->orderBy('id', 'desc')->paginate(15);

        return view('srp.index', compact('srps'));
    }

    public function allSrps() {
        $srps = ShipReplacement::where('status', 'OPEN')
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('srp.allsrps', compact('srps'));
    }

    public function approvedSRPs(){
        $srps = ShipReplacement::where('status', 'APPROVED')->orderBy('id', 'desc')->paginate(15);
        return view('srp.approvedSRPs', compact('srps'));
    }

    public function closedSrps() {
        $srps = ShipReplacement::where('status', 'CLOSED')->orderBy('id', 'desc')->paginate(15);
        return view('srp.closedSRPs', compact('srps'));
    }

    public function deniedSrps() {
        $srps = ShipReplacement::where('status', 'DENIED')->orderBy('id', 'desc')->paginate(15);
        return view('srp.deniedSRPs', compact('srps'));
    }

    public function sortResults(){
        $view = $_GET['view'];
        $sortBy = $_GET['sortBy'];
        $status = $_GET['status'];
        $srps = ShipReplacement::where('status', $status)->orderBy($sortBy, 'desc')->paginate(15);
        return view('srp.'.$view, compact('srps'));
    }

    public function create() {

        $characterName = Session::get('charName');
        $fleetsAttended = FleetParticipant::where('characterName', $characterName)->paginate(15);

        return view('srp.create', compact('fleetsAttended'));
    }

    public function file($id){
        $fleetDate = Fleet::where('id', $id)->value('created_at');

        $characterID = Session::get('charId');
        $endTime = $fleetDate->addHours(3)->toDateTimeString();
        $startTime = $fleetDate->subHours(3)->toDateTimeString();

        $killInfo = (new KillmailForSrp())->buildUrl($characterID, $startTime, $endTime);
        if ($killInfo) {
            $this->store(null, $killInfo[0]);
            return back();
        } else {
            Session::flash('flash_error_message', 'Congrats! It looks like you didn\'t die during this fleet. Contact a director if you feel like you are getting this by mistake.');
            return back();
        }

    }

    public function store(Requests\SRPRequest $request = null, $killID = null) {
        if ($request) {
            $input = $request->all();
            $input['updated_at'] = Carbon::now();

            $link = $request->zKillBoardLink;
            $pattern = '/([0-9])\w+/';
            preg_match($pattern, $link, $killID);
        }
        $killID = $killID[0];
        $killInfo = (new Killmail($killID))->connect();
        //dd($killInfo);
        $shipType = $this->getShipName($killInfo[0]['victim']['shipTypeID']);
        $systemName = $this->getSystemName($killInfo[0]['solarSystemID']);

        $input['killID'] = $killInfo['0']['killID'];
        $input['solarSystemID'] = $systemName;
        $input['fleetDate'] = $killInfo[0]['killTime'];
        $input['shipType'] = $shipType;
        $input['totalValue'] = $killInfo[0]['zkb']['totalValue'];
        if(!empty($killInfo[0]['victim']['characterName'])){
            $input['inGameName'] = $killInfo[0]['victim']['characterName'];
        } else {
            $char = Session::get('charName');
            $input['inGameName'] = $char;
        }

        (new SlackHook())->encodeForSlack($input);
        ShipReplacement::create($input);
        Session::flash('flash_message', 'Your claim has been submitted.');
        return back();
    }

    public function update(ShipReplacement $srp, Request $request) {
        $srp->update($request->only(['status', 'note']));
        Session::flash('flash_message', 'The SRP request has been updated.');
        return back();
    }

    public function reopenSRP(ShipReplacement $srp, Request $request) {
        $srp->update($request->only(['status']));
        Session::flash('flash_message', 'The SRP request has been reopened.');
        return back();
    }

    private function getShipName($shipTypeID) {
        $shipName = Invtype::where('typeID', $shipTypeID)->value('typeName');

        return $shipName;
    }

    private function getSystemName($solarSystem) {
        $solarSystemName = Mapsolarsystems::where('solarSystemID', $solarSystem)->value('solarSystemName');

        return $solarSystemName;
    }

/*    public function updateVictim(){
        $kills = ShipReplacement::all();
        //dd($kills);
        foreach($kills as $kill){

            $info = (new Killboard($kill->killID))->connect();
            $vicName = $info[0]['victim']['characterName'];


            ShipReplacement::where('id', $kill->id)->update(['inGameName' => $vicName ]);
        }

        return back();
    }*/
}
