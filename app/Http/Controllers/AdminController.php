<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    public function index() {
        $users = User::all();
        return view('admin.index', compact('users'));
    }

    public function update($id, Request $request) {
        User::where('id', $id)->update(['group' => $request->group]);
        return back();
    }

}
