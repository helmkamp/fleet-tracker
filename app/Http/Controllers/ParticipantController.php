<?php

namespace App\Http\Controllers;

use App\Http\Connection\Connection;
use App\Participant;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;

class ParticipantController extends Controller
{
    public function index() {
        $this->getLocation();
        return view('index');
    }

    public function store(Request $request) {
        $input = $request->all();
        $input['updated_at'] = Carbon::now();

        Participant::create($input);
        Session::flash('flash_message', 'Your participation has been recorded.');
        return redirect('index');
    }

    protected function getLocation() {
        $user = Session::get('charId');

        $url = 'https://crest-tq.eveonline.com/characters/' . $user . '/location/';
        $options = array(
            CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . Session::get('token'),
                    'Accept: application/vnd.ccp.eve.Api-v3+json',
                    'Host: crest-tq.eveonline.com',
                    "User-Agent: Fleet Tracker Contact/helmy@threebeersaday.com"
                ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        );
        $loc = new Connection();
        $json = $loc->connect($options);

        if($json != null) {
            Session::put('systemName', $json['solarSystem']['name']);
            if (array_key_exists('station', $json)) {
                Session::put('stationName', $json['station']['name']);
            }
        }
        else {
            return redirect('/errors/wrongcorp');
        }
    }
}
