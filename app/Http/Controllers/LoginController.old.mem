<?php


namespace App\Http\Controllers;


use App\Http\Connection\Connection;
use App\Http\Connection\LoginUrlBuilder;
use App\User;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{


    public function index()
    {

        // the user has logged in already take them to the Welcome page
        if(Session::has('refresh_token')) {
            //Session::flush();
            $this->refreshLogin();
            return redirect('welcome');
        }

        return redirect((new LoginUrlBuilder())->builder());
    }

    public function authLogin() {

        $code = $_GET['code'];
        $body = 'grant_type=authorization_code&code=' . $code;
        $b64En = base64_encode(env('CLIENT_ID') . ':' . env('CLIENT_SECRET'));

        $url = 'https://login.eveonline.com/oauth/token';

        $options = array( CURLOPT_HTTPHEADER => array(
            'Authorization: Basic ' . $b64En,
            'Content-Type: application/x-www-form-urlencoded',
            'Host: login.eveonline.com',
            "User-Agent: Contact/helmy@threebeersaday.com"
            ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $body
        );

        $authentication = (new Connection())->connect($options);


        // Set session variables for the access token and refresh token
        Session::put('token', $authentication['access_token']);
        Session::put('refresh_token', $authentication['refresh_token']);

        // verify the character
        $this->verifyCharacter($authentication['access_token']);

        $corp = Session::get('corporation');
        //dd($corp);
        // If they aren't a member of MooseArmy redirect them to an error page
        if($corp === "Moosearmy" || $corp === "Moosearmy Heavy Industries") {
            // Extract to it's own class
            // Save the user in the database if they aren't already
            $user['CharacterName'] = Session::get('charName');
            $user['CharacterID'] = Session::get('charId');
            $user['CharacterOwnerHash'] = Session::get('ownerHash');
            $this->saveUser($user);
                
            return view('welcome');
        }

        Session::flush();
        return redirect('/errors/wrongcorp');

    }

    public function saveUser($json) {
        $isRegistered = User::where('characterOwnerHash', $json['CharacterOwnerHash'])->first();

        if(!$isRegistered) {
            $character['characterName'] = $json['CharacterName'];
            $character['characterID'] = $json['CharacterID'];
            $character['characterOwnerHash'] = $json['CharacterOwnerHash'];
            $character['updated_at'] = Carbon::now();

            User::create($character);
            return view('welcome');
        }

        Session::put('group', $isRegistered->group);
    }

    public function verifyCharacter($token) {
        $url = 'https://login.eveonline.com/oauth/verify';
        $options = array(
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token,
                'Content-Type: application/x-www-form-urlencoded',
                'Host: login.eveonline.com',
                "User-Agent: Fleet Tracker Contact/helmy@threebeersaday.com"
            ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        );

        $character = (new Connection())->connect($options);

        // Store the character info for later use
        Session::put('charName', $character['CharacterName']);
        Session::put('charId', $character['CharacterID']);
        Session::put('ownerHash', $character['CharacterOwnerHash']);
        // Grab the character publicData and Location
        $this->getCharacterInfo($character);

    }

    public function getCharacterInfo($character) {
        $url = 'https://crest-tq.eveonline.com/characters/' . $character['CharacterID'] . '/';

        $options = array(
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . Session::get('token'),
                'Accept: application/vnd.ccp.eve.Api-v3+json',
                'Host: crest-tq.eveonline.com',
                "User-Agent: Fleet Tracker Contact/helmy@threebeersaday.com"
            ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        );

        $info = (new Connection())->connect($options);

        Session::put('corporation', $info['corporation']['name']);
        Session::put('corpImage', $info['corporation']['logo']['64x64']['href']);

    }

    public function refreshLogin() {

        $body = 'grant_type=refresh_token&refresh_token=' . Session::get('refresh_token');
        $b64En = base64_encode(env('CLIENT_ID') . ':' . env('CLIENT_SECRET'));

        $url = 'https://login.eveonline.com/oauth/token';

        $options = array(
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . $b64En,
                'Content-Type: application/x-www-form-urlencoded',
                'Host: login.eveonline.com',
                "User-Agent: Fleet Tracker Contact/helmy@threebeersaday.com"
            ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $body
        );

        $refresh = (new Connection())->connect($options);

        // Set session variables with the new access token and refresh token
        Session::put('token', $refresh['access_token']);
        Session::put('refresh_token', $refresh['refresh_token']);

        // verify the character
        $this->verifyCharacter($refresh['access_token']);

    }

    public function logout() {
        Session::flush();
        return redirect('/');
    }
}

