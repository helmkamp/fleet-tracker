<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Fleet;
use App\FleetParticipant;
use App\Http\Requests;
use App\Http\Connection\Connection;

class FleetController extends Controller
{
    public function index(){
        $fleets = Fleet::all();
        return view('fleet.index', compact('fleets'));
    }

    public function create(Request $request) {

        $participants = $this->getFleetInfo($request->link);

        Session::put('fleet', $participants);
        return view('fleet.addfleet', compact('participants'));
    }

    public function store(Request $request) {

        // Build Fleet Record and save it to the database
        $fleetInput['characterName'] = $request->characterName;
        $fleetInput['fleetName'] = $request->fleetName;
        $fleetInput['fleetCommander'] = $request->fleetCommander;

        Fleet::create($fleetInput);
        // Pull the ID from the last Fleet record
        $fleet_id = Fleet::all()->last();
        // Build individual fleet participant record and save it to the DB

        $participants = Session::get('fleet');
        foreach($participants as $participant) {
            $participant['fleet_id'] = $fleet_id->id;
            FleetParticipant::create($participant);
        }
        Session::flash('flash_message', 'A new fleet record has been created.');
        return back();
    }

    public function viewFleet($id) {
        $fleet = Fleet::where('id', $id)->get()->first();
        $participants = Fleet::find($id)->fleetParticipant;

        return view('fleet.fleet', compact('fleet', 'participants'));
    }

    public function viewLink() {
        return view('fleet.link');
    }

    protected function getFleetInfo($link) {
        $token = Session::get('token');
        $url = $link . "members/";
        $fleet = new Connection();
        $options = array(
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token,
                'Content-Type: application/x-www-form-urlencoded',
                'Host: login.eveonline.com',
                "User-Agent: Fleet Tracker Contact/helmy@threebeersaday.com"
            ),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        );
        $fleetInfo = $fleet->connect($options);
        //dd($fleetInfo);
        $newFleet = $this->setFleetInfo($fleetInfo);

        return $newFleet;
    }

    protected function setFleetInfo($fleetInfo) {
        $newFleet = array();

        foreach($fleetInfo['items'] as $participant){
            $record = array(
                "characterName" => $participant['character']['name'],
                "location" => $participant['solarSystem']['name'],
                "shipType" => $participant['ship']['name'],
                "fleetRole" => $participant['roleName']
            );
            $newFleet[] = $record;
        }

        return $newFleet;
    }
}

/*
Keolae Moliko	FAT-6P (Docked)	Rattlesnake	Battleship	Squad Commander	0 - 0 - 5	FAT / Squad 6
WaypointExit	CNC-4V (Docked)	Miasmos	Industrial	Squad Member	0 - 0 - 5	FAT / Squad 6

https://crest-tq.eveonline.com/fleets/1093111235822/
 */
