<?php


namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Http\Requests\Request;

class SlackHook
{
    public function __construct()
    {
    }

    // Take the SRP request info and format it in JSON to be sent through cURL
    public function encodeForSlack($request)
    {
        $price = number_format($request['totalValue'], 2, '.', ',');
        $time = Carbon::now();

        // Setup the array to be JSON encoded
        $arr = array('attachments' => array(array(
            'fallback' => 'This is a message from the Fleet Tracker Site.',
            'title' => 'A new SRP request has been submitted.',
            'title_link' => 'https://mytvindex.com',
            'text' => 'The details are as follows:',
            'fields' => array(
                array(
                    'title' => 'In-game Name',
                    'value' => $request['inGameName'],
                    'short' => true
                ),
                array(
                    'title' => 'Ship Type',
                    'value' => $request['shipType'],
                    'short' => true
                ),
                array(
                    'title' => 'System Name',
                    'value' => $request['solarSystemID'],
                    'short' => true
                ),
                array(
                    'title' => 'Fleet Date',
                    'value' => $request['fleetDate'],
                    'short' => true
                ),
                array(
                    'title' => 'FC Name',
                    'value' => $request['fcName'],
                    'short' => true
                ),
                array(
                    'title' => 'Estimated Value',
                    'value' => $price,
                    'short' => true
                ),
                array(
                    'title' => 'zKillBoard Link',
                    'value' => $request['zKillBoardLink'],
                    'short' => true
                ),
                array(
                    'title' => 'Timestamp',
                    'value' => $time->toDateTimeString(),
                    'short' => true
                )
            ))
        ));

        // Encode the array to JSON
        $json = json_encode($arr);

        // Pass on the JSON object to be sent with cURL
        $this->sendSlackMessage($json);
    }

    // Accepts a JSON object to be sent to Slack through cURL
    private function sendSlackMessage($json)
    {

        $url = 'https://hooks.slack.com/services/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';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json"
            )
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error) {
            throw new \Exception($error);
        }
    }
}