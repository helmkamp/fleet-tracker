<?php


Route::get('/', function () {
    return view('auth.login');
});
Route::get('welcome', function () {
    return view('welcome');
});
Route::get('errors/wrongcorp', function() {
   return view('errors.wrongcorp');
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::resource('srp', 'SRPController');
    Route::resource('admin', 'AdminController');
    Route::get('allsrps', 'SRPController@allSrps');
    Route::get('closedSRPs', 'SRPController@closedSrps');
    Route::get('deniedSRPs', 'SRPController@deniedSrps');
    Route::get('approvedSRPs', 'SRPController@approvedSrps');
    Route::get('sort', 'SRPController@sortResults');
    Route::patch('allsrps/{srp}', 'SRPController@update');
    Route::patch('closedSRPs/{srp}', 'SRPController@reopenSRP');
    Route::get('login', 'LoginController@index');
    Route::get('logout', 'LoginController@logout');
    Route::get('authLogin', 'LoginController@authLogin');
    Route::get('index', 'ParticipantController@index');
    Route::post('index', 'ParticipantController@store');
    Route::get('fleets', 'FleetController@index');
    Route::get('fleets/{id}', 'FleetController@viewFleet');
    Route::post('link', 'FleetController@create');
    Route::post('addfleet', 'FleetController@store');
    Route::get('link', 'FleetController@viewLink');
    Route::get('fileSRP/{srp}', 'SRPController@file');
//    Route::get('updateVictimName', 'SRPController@updateVictim');
});
