<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fleet extends Model
{
    protected $fillable = [
        'fleetName',
        'characterName',
        'fleetCommander',
        'created_at',
        'updated_at'
    ];

    public function fleetParticipant() {
        return $this->hasMany(FleetParticipant::class);
    }
}
