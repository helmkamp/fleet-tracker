<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
        'systemName',
        'pilotName',
        'shipType',
        'corporation',
        'docked',
        'updated_at',
        'created_at'
    ];
}
