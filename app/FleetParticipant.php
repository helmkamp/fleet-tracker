<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FleetParticipant extends Model
{
    protected $fillable = [
        'fleet_id',
        'characterName',
        'location',
        'shipType',
        'shipGroup',
        'fleetRole',
        'fleetSkills',
        'fleetPosition',
        'created_at',
        'updated_at'
    ];

    public function fleet() {
        return $this->belongsTo(Fleet::class);
    }
}