<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipReplacement extends Model
{
    protected $fillable = [
        'inGameName',
        'characterOwnerHash',
        'shipType',
        'zKillBoardLink',
        'fleetDate',
        'fcName',
        'killID',
        'solarSystemID',
        'totalValue',
        'status',
        'note',
        'created_at',
        'updated_at'
    ];
}
