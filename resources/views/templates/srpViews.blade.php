@if($srps)
    <table class="table table-striped table-condensed table-responsive table-hover" >
        <tbody>
        <tr>
            <th><a href="/sort?view={{ $view }}&sortBy=inGameName&status={{ $status }}" class="text-info">In-game Name</a></th>
            <th><a href="/sort?view={{ $view }}&sortBy=shipType&status={{ $status }}" class="text-info">Ship Type</a></th>
            <th><a href="/sort?view={{ $view }}&sortBy=solarSystemID&status={{ $status }}" class="text-info">Solar System</a></th>
            <th><a href="/sort?view={{ $view }}&sortBy=fleetDate&status={{ $status }}" class="text-info">Fleet Date*</a></th>
            <th><a href="/sort?view={{ $view }}&sortBy=fcName&status={{ $status }}" class="text-info">FC Name</a></th>
            <th><a href="/sort?view={{ $view }}&sortBy=totalValue&status={{ $status }}" class="text-info">Estimated Value</a></th>
            <th>zKillBoard Link</th>
            <th>Note/Status</th>

        </tr>
        @foreach($srps as $srp)
            <tr>
                <td style="column-width: 200px">{{$srp->inGameName}}</td>
                <td>{{$srp->shipType}}</td>
                <td>{{$srp->solarSystemID}}</td>
                <td>{{$srp->fleetDate}}</td>
                <td>{{$srp->fcName}}</td>
                <td>{{number_format($srp->totalValue, 2, '.', ',')}}</td>
                <td><a href="{!! $srp->zKillBoardLink !!}" target="_blank">Kill Report</a> </td>
                <div >
                {{ Form::model($srp, array('route' => array('srp.update', $srp->id))) }}
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <td style="column-width: 250px">{{ Form::text('note', null, ['class' => 'input-sm form-control']) }}</td>
                </div>
                <div class="form-group">
                    <td style="column-width: 120px">{{ Form::select('status', ['OPEN' => 'Open', 'APPROVED' => 'Approved', 'DENIED' => 'Denied', 'CLOSED' => 'Closed'],
                            $srp->status, ['class' => 'input-sm form-control']) }}</td>
                </div>

                <td><input type="submit" value="Update" class="btn btn-primary btn-sm"/></td>
                {{ Form::close() }}
                </div>
            </tr>
        @endforeach

        </tbody>
    </table>
    @if(isset($_GET['view']))
        {!! $srps->appends(['view' => $_GET['view'], 'sortBy' => $_GET['sortBy'], 'status' => $_GET['status']])->render() !!}
    @else
        {!! $srps->links() !!}
    @endif
@else
    <p>You have no SRP requests</p>
@endif
<br>
<div>
    <p>*Fleet date/times are recorded in Eve time.</p>
</div>