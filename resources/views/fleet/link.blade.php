@extends('layout')
@section('head')
    <h1 class="page-header">Fleet Link</h1>
@stop
@section('lead')
    <p class="lead">Add the external link to continue</p>
@stop
@section('content')
    <h4>You must be the fleet commander to add a fleet.</h4>
    <form method="POST" action="{{ url('/link') }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="form-group">
            <label for="link" class="control-label col-lg-2">External Link: </label>
            <div class=" col-lg-5">
                <input type="text" name="link" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-6 col-lg-offset-2">
                <input type="submit" value="Submit" class="btn-primary btn">
            </div>
        </div>
    </form>


@stop