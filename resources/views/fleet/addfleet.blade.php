@extends('layout')
@section('head')
    <h1 class="page-header">Add a New Fleet</h1>
@stop
@section('lead')
    <p class="lead">New fleet details</p>
@stop
@section('content')

    <form method="POST" action="{{ url('/addfleet') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="characterName" value="{{Session::get('charName')}}"/>
        <div class="form-group">
            <label for="fleetName" class="control-label">Fleet Name</label>
            <input type="text" name="fleetName" class="form-control" value="" placeholder="March 30 NPSI Roam"/>
        </div>
        <div class="form-group">
            <label for="fleetCommander" class="control-label">Fleet Commander</label>
            <input type="text" name="fleetCommander" class="form-control" value="" placeholder="Moose"/>
        </div>
        {{--<div class="form-group">
            <label for="fleet">Fleet (copy/paste from the Fleet Comp window)</label>
            <textarea name="fleet" id="fleet" cols="30" rows="10" class="form-control"></textarea>
        </div>--}}
        <div>
            <table class="table table-striped table-condensed">
                <tbody>
                <tr>
                    <th>Participant Name</th>
                    <th>Location</th>
                    <th>Ship Type</th>
                    <th>Fleet Role</th>
                </tr>
                @foreach($participants as $participant)
                    <tr>
                        <td>{{$participant['characterName']}}</td>
                        <td>{{$participant['location']}}</td>
                        <td>{{$participant['shipType']}}</td>
                        <td>{{$participant['fleetRole']}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-primary"/>
        </div>
    </form>


@stop