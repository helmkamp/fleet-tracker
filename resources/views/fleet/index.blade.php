@extends('layout')
@section('head')
    <h1 class="page-header">Fleets</h1>
@stop
@section('lead')
    <p class="lead">Available Fleets</p>
@stop
@section('content')

    <div class="panel panel-default col-md-5">
        <div class="panel-heading">Choose a fleet to see its details</div>
        <div class="panel-body">
            <ul class="list-group">
                @foreach($fleets as $fleet)
                    <li class="list-group-item"><a href="/fleets/{{$fleet->id}}">{{$fleet->fleetName}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@stop