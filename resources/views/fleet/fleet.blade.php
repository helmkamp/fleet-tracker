@extends('layout')
@section('head')
    <h1 class="page-header">Fleet View</h1>
@stop
@section('lead')
    <p class="lead">{{ $fleet->fleetName }}</p>
@stop
@section('content')

    <table class="table table-condensed">
        <tbody>
            <tr>
                <th>Operation Name</th>
                <th>Fleet Commander</th>
                <th>Date</th>
                <th>Entered By</th>
            </tr>
            <tr>
                <td>{{$fleet->fleetName}}</td>
                <td>{{$fleet->fleetCommander}}</td>
                <td>{{$fleet->created_at}}</td>
                <td>{{$fleet->characterName}}</td>
            </tr>
        </tbody>

    </table>
    <table class="table table-striped table-condensed">
        <tbody>
        <tr>
            <th>Participant Name</th>
            <th>Location</th>
            <th>Ship Type</th>
            <th>Fleet Role</th>
        </tr>
        @foreach($participants as $participant)
            <tr>
                <td>{{$participant->characterName}}</td>
                <td>{{$participant->location}}</td>
                <td>{{$participant->shipType}}</td>
                <td>{{$participant->fleetRole}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop