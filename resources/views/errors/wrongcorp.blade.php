
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="A web app made for the Eve Online MooseArmy Corporation to track fleet participation and the SRP.">
    <meta name="author" content="Andrew Helmkamp">
    {{--<link rel="icon" href="../../favicon.ico">--}}

    <title>Wrong Corporation</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/signin.css" rel="stylesheet">


    <script src="/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body style="background-color: #1c1c1c">

<div class="container">

    <div style="color: #888a85" class="center-block">
        <img class="center-block" src="/img/eightbit.jpg" alt="Moose Army Logo" width="50%">

            <p style="text-align: center">We're sorry, but this site is for Moose Federation members only.</p>
            <p style="text-align: center">You can go <a href="http://forum.moosehouseshow.com/showthread.php?tid=25">here</a> for more information on how to become
                part of the MooseArmy!</p>

    </div>
</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>
{{--@if($_SERVER['HTTP_EVE_TRUSTED'] == 'No')--}}
{{--<script>--}}
{{--CCPEVE.requestTrust('https://mytvindex.com/*');--}}
{{--</script>--}}
{{--@endif--}}
</body>
</html>