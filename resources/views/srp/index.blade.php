@extends('layout')
@section('head')
    <h1 class="page-header">Ship Replacement Requests</h1>
@stop
@section('lead')
    <p class="lead">A list of your SRP Requests</p>
@stop
@section('content')

    @if($srps)
        <table class="table table-striped table-condensed">
            <tbody>
            <tr>
                <th>In-game Name</th>
                <th>Ship Type</th>
                <th>Solar System</th>
                <th>Fleet Date</th>
                <th>FC Name</th>
                <th>Estimated Value</th>
                <th>zKillBoard Link</th>
                <th>Status</th>
                <th>Notes</th>
            </tr>
            @foreach($srps as $srp)
                <tr>
                    <td>{{$srp->inGameName}}</td>
                    <td>{{$srp->shipType}}</td>
                    <td>{{$srp->solarSystemID}}</td>
                    <td>{{$srp->fleetDate}}</td>
                    <td>{{$srp->fcName}}</td>
                    <td>{{number_format($srp->totalValue, 3, '.', ',')}}</td>
                    <td><a href="{!! $srp->zKillBoardLink !!}" target="_blank">Kill Report</a> </td>
                    <td>{{$srp->status}}</td>
                    <td>{{$srp->note}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
        {{ $srps->render() }}
    @else
        <p>You have no SRP requests</p>
    @endif
    <br>
    <div>
        <p>*Fleet date/times are recorded in Eve time.</p>
    </div>

@stop