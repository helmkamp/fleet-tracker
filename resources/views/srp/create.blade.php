@extends('layout')
@section('head')
    <h1 class="page-header">Ship Replacement Request</h1>
@stop
@section('lead')
    <p class="lead">Create a SRP Request</p>
@stop
@section('content')
    <link href="/css/custom.css" rel="stylesheet">

    {{--@if($fleetsAttended)
        <table class="table table-striped table-condensed">
            <tbody>
            <tr>
                --}}{{--<th>Fleet ID</th>--}}{{--
                <th>Location</th>
                <th>Ship</th>
                <th>Fleet Role</th>
                <th>Date</th>
                <th>Submit SRP</th>
            </tr>
            @foreach($fleetsAttended as $fleetAttended)
                <tr>
                    --}}{{--<td>{{$fleetAttended->fleet_id}}</td>--}}{{--
                    <td>{{$fleetAttended->location}}</td>
                    <td>{{$fleetAttended->shipType}}</td>
                    <td>{{$fleetAttended->fleetRole}}</td>
                    <td>{{$fleetAttended->created_at}}</td>
                    <td><a href="/fileSRP/{{ $fleetAttended->fleet_id }}" class="srpLink">Submit</a></td>
                </tr>
            @endforeach

            </tbody>
        </table>
        {{ $fleetsAttended->render() }}
    @else
        <p>You have no SRP requests</p>
    @endif
    <br>
    <div>
        <p>*Fleet date/times are recorded in Eve time.</p>
    </div>
    <br><hr>
    <h3>Manual Input</h3>
    <p>If you were part of an alliance fleet that is approved for SRP you can enter your information here.</p>
    --}}
    <form method="POST" action="{{ url('/srp') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="characterOwnerHash" value="{{Session::get('ownerHash')}}"/>
        <div class="form-group">
            <label for="zKillBoardLink" class="control-label">zKillBoard Link</label>
            <input type="text" name="zKillBoardLink" class="form-control">
            <p style="padding-top: 10px"><a href="http://zkillboard.com" target="_blank">www.zKillBoard.com</a></p>
        </div>
        <div class="form-group">
            <label for="fcName" class="control-label">FC Name</label>
            <input type="text" name="fcName" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-primary">
        </div>
    </form>

@stop