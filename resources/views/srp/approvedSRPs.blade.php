@extends('layout')
@section('head')
    <h1 class="page-header">Ship Replacement Program</h1>
@stop
@section('lead')
    <p class="lead">Approved SRP Requests</p>
@stop
@section('content')

    @include('templates.srpViews', ['view' => 'approvedSRPs', 'status' => 'APPROVED'])


@stop