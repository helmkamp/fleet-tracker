@extends('layout')
@section('head')
    <h1 class="page-header">Ship Replacement Program</h1>
@stop
@section('lead')
    <p class="lead">Open SRP Requests</p>
@stop
@section('content')

    @include('templates.srpViews', ['view' => 'allsrps', 'status' => 'OPEN'])
    <br><br><br>
    {{--<a href="/updateVictimName"><h1>UPDATE</h1></a>--}}
@stop


