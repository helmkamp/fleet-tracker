@extends('layout')
@section('head')
    <h1 class="page-header">Fleet Tracker</h1>
@stop
@section('lead')
    <p class="lead">The information below will be recorded for your attendance.</p>
@stop
@section('content')




    <div class="panel panel-default">
        <div class="panel-body">
            {{--<form method="POST" action="{{ url('/index') }}">--}}
                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="pilotName" class="control-label">{{$_SERVER['HTTP_EVE_CHARNAME']}}</label>--}}
                    {{--<input type="hidden" name="pilotName" value="{{$_SERVER['HTTP_EVE_CHARNAME']}}"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="systemName" class="control-label">{{$_SERVER['HTTP_EVE_SOLARSYSTEMNAME']}}</label>--}}
                    {{--<input type="hidden" name="systemName" value="{{$_SERVER['HTTP_EVE_SOLARSYSTEMNAME']}}"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="shipType" class="control-label">{{$_SERVER['HTTP_EVE_SHIPTYPENAME']}}</label>--}}
                    {{--<input type="hidden" name="shipType" value="{{$_SERVER['HTTP_EVE_SHIPTYPENAME']}}"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="corporation" class="control-label">{{$_SERVER['HTTP_EVE_CORPNAME']}}</label>--}}
                    {{--<input type="hidden" name="corporation" value="{{$_SERVER['HTTP_EVE_CORPNAME']}}"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--@if($_SERVER['HTTP_EVE_STATIONNAME'])--}}
                        {{--<label for="docked" class="control-label">Docked</label>--}}
                        {{--<input type="hidden" name="docked" value="Docked"/>--}}
                    {{--@else--}}
                        {{--<label for="docked" class="control-label">Undocked</label>--}}
                        {{--<input type="hidden" name="docked" value="Undocked"/>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<input type="submit" value="Submit" class="btn btn-primary"/>--}}
                {{--</div>--}}
                {{--<p>Please refresh the page if the only thing showing is 'Undocked'.</p>--}}
            {{--</form>--}}

            @if(Session::get('systemName') != null)
            <form method="POST" action="{{ url('/index') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="pilotName" class="control-label">{{ Session::get('charName') }}</label>
                    <input type="hidden" name="pilotName" value="{{ Session::get('charName') }}"/>
                </div>
                <div class="form-group">
                    <label for="systemName" class="control-label">{{ Session::get('systemName') }}</label>
                    <input type="hidden" name="systemName" value="{{ Session::get('systemName') }}"/>
                </div>
                @if(Session::has('stationName'))
                    <div class="form-group">
                        <label for="docked" class="control-label">{{ Session::get('stationName') }}</label>
                        <input type="hidden" name="docked" value="{{ Session::get('stationName') }}"/>
                    </div>
                @else
                    <div class="form-group">
                        <label for="docked" class="control-label">Undocked</label>
                        <input type="hidden" name="docked" value="Undocked"/>
                    </div>
                @endif
                <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
            @else
                <p>It looks like you aren't logged into Eve. Please login and try again.</p>
            @endif
        </div>
    </div>

@stop
