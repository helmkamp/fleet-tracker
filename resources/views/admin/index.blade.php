@extends('layout')
@section('head')
    <h1 class="page-header">Users</h1>
@stop
@section('lead')
    <p class="lead">Edit a user's permissions</p>
@stop
@section('content')
    <div class="panel panel-default col-md-6">
        <div class="panel-body">
            <input id="searchInput" value="Type to filter" class="form-control">
            <table class="table table-striped table-condensed">
                <tbody id="fbody">
                    <tr>
                        <th>Character Name</th>
                        <th>User Group</th>
                        <th>Update</th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->characterName}}</td>
                            {!! Form::model($user,['method' => 'PATCH', 'route' => ['admin.update', $user->id]])!!}
                                <td>{!! Form::select('group', ['user'=>'user','admin'=>'admin'], $user->group, ['class' => 'form-control input-sm']) !!}</td>
                                <td>{!! Form::submit('Submit', ['class' => 'btn btn-primary btn-sm']) !!}</td>
                            {!! Form::close() !!}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @endsection
@section('footer')
    <script src="/js/adminFilter.js"></script>
@stop