
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="A web app made for the Eve Online MooseArmy Corporation to track fleet participation and the SRP.">
    <meta name="author" content="Andrew Helmkamp">
    {{--<link rel="icon" href="../../favicon.ico">--}}

    <title>MooseArmy Fleet Tracker</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/slate.min.css" rel="stylesheet">
    <link href="/css/srpTables.css" rel="stylesheet">


    <script src="/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

                <img class="pull-left navbar-brand" src="{{ Session::get('corpImage') }}" alt="Corporation Image">
                <p class="navbar-brand">Welcome, {{ Session::get('charName') }}</p>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                {{--<li><a href="#">Profile</a></li>--}}
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="page-header">
        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-6">
                @yield('head')
                @yield('lead')
            </div>
        </div>
    <div class="row">
        {{--<div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="/index">Track Participation</a></li>
                <li><a href="/srp">Your SRP Requests</a></li>
                <li><a href="/srp/create">File SRP Request</a></li>
                <li><a href="/link">Add Fleet Log</a></li>
                @if(Session::get('group') === 'admin')
                    <li><a href="/fleets">View Fleets</a></li>
                    <li><a href="/allsrps">Open SRP Requests</a></li>
                    <li><a href="/approvedSRPs">Approved SRP Requests</a></li>
                    <li><a href="/deniedSRPs">Denied SRP Requests</a></li>
                    <li><a href="/closedSRPs">Closed SRP Requests</a></li>
                    <li><a href="/admin">Users</a> </li>
                @endif
            </ul>
        </div>--}}
        <div class="col-lg-3 col-md-3 col-sm-4">
            <div class="list-group table-of-contents">
                <span class="list-group-item text-center"><strong>Ship Replacement</strong></span>
                <a class="list-group-item" href="/srp">Your SRP Requests</a>
                <a class="list-group-item" href="/srp/create">File SRP Request</a>
            </div>
            <div class="list-group table-of-contents">
                <span class="list-group-item text-center"><strong>Fleet Tracking</strong></span>
                <a class="list-group-item" href="/index">Track Participation</a>
                <a class="list-group-item" href="/link">Add Fleet Log</a>
            </div>
            @if(Session::get('group') === 'admin')
                <div class="list-group table-of-contents">
                    <span class="list-group-item text-center"><strong>Administration</strong></span>
                    <a class="list-group-item" href="/fleets">View Fleets</a>
                    <a class="list-group-item" href="/allsrps">Open SRP Requests</a>
                    <a class="list-group-item" href="/approvedSRPs">Approved SRP Requests</a>
                    <a class="list-group-item" href="/deniedSRPs">Denied SRP Requests</a>
                    <a class="list-group-item" href="/closedSRPs">Closed SRP Requests</a>
                    <a class="list-group-item" href="/admin">Users</a>
                </div>
            @endif
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 main">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">
                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            @if(Session::has('flash_error_message'))
                <div class="alert alert-danger">
                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_error_message') }}
                </div>
            @endif
            @yield('content')
        </div>
    </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery.min.js"><\/script>')</script>
{{--<script src="/js/vue.js"></script>--}}
<script src="/js/bootstrap.min.js"></script>
<script src="/js/accounting.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>
@yield('footer')
</body>
</html>