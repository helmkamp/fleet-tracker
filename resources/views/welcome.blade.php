@extends('layout')
@section('head')
    <h1 class="page-header">Welcome</h1>
@stop
{{--@section('lead')
    <p class="lead">Edit a user's permissions</p>
@stop--}}
@section('content')

    @if(Session::has('charName'))
    <div>
        <h3 class="center-block">Welcome {{Session::get('charName')}} to the Moose Federation Fleet Tracker and SRP Site.</h3>
        <h4 class="center-block">Please make a selection from the side menu.</h4>
    </div>
    @else
        <div>
            <h3 class="center-block">Welcome to the Moose Federation Fleet Tracker and SRP Site.</h3>
            <h4 class="center-block">Please make a selection from the side menu.</h4>
        </div>
    @endif

@stop