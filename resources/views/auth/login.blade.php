<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="A web app made for the Eve Online MooseArmy Corporation to track fleet participation and the SRP.">
    <meta name="author" content="Andrew Helmkamp">
    {{--<link rel="icon" href="../../favicon.ico">--}}
    <title>Signin To Moose Federation Fleet Tracker</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/css/signin.css" rel="stylesheet">

    <script src="/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
    <div class="center-block">

    </div>
</div> <!-- /container -->
<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    {{--<h3 class="masthead-brand">Moosearmy</h3>--}}
                    {{--<nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </nav>--}}
                </div>
            </div>

            <div class="inner cover">
                <h1 class="cover-heading">Moose Federation</h1>
                <h1 class="cover-heading">Fleet Tracker and SRP</h1>
                <br/>
                <p class="lead">
                    <a href="login"> <img src="/img/EVE_SSO_Login_Buttons_Large_Black.png" alt="Eve Online Sign-in" class="center-block"></a>
                </p>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Join <a href="http://moosefed.com">Moose Federation.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>
{{--@if($_SERVER['HTTP_EVE_TRUSTED'] == 'No')--}}
    {{--<script>--}}
        {{--CCPEVE.requestTrust('https://mytvindex.com/*');--}}
    {{--</script>--}}
{{--@endif--}}
</body>
</html>
